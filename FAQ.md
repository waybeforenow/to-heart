#to-heart FAQ

[[_TOC_]]


## What is ToHeart?

ToHeart is an adult adventure game originally published by Leaf and Aquaplus in 1997. It is the third and final entry in the Leaf Visual Novel Series (LVNS). It quickly became a cult success upon its release and spawned several anime series and OVA, a manga, various fangames, ports and re-releases. It also received a sequel, ToHeart 2, with entirely new characters.


## For what platforms was ToHeart released?

Officially:
* "ToHeart", Windows (1997)
* "ToHeart", PlayStation 1 (1999)
  * This version of the game adds voice acting and mini-games.
  * Additionally, erotic content was removed, and the script was significantly revised.
* "ToHeart PSE", Windows (2003)
  * This is a Windows release of the PlayStation port. There is no erotic content.
* "ToHeart & ToHeart2 Limited Deluxe Pack", PlayStation 2 (2004)
* "ToHeart Portable", PlayStation Portable (2009)

Unoffically:
* There is an asset converter for the cross-platform [Digital Novel Markup Language](http://mak165165.starfree.jp/list/list_toheart.htm) (199?)
* "[xlvns](https://github.com/waybeforenow/xlvns"), UNIX (2001)
* [Game Boy Color](http://mydocuments.g2.xrea.com/html/gb/thgb.html) (2017)


## Which version of the game is being translated?

The original 1997 PC version of the game is currently being translated.


## What is the status of the translation?

The translation has been ongoing since at least 2018. There is no planned release date.
